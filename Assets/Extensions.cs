﻿using System.Collections;
using UnityEngine;

public static class Extensions
{
  public static Quaternion TopDownLookAt(this Transform self, Vector2 target, float angleOffset = 0)
  {
    Vector2 dir = target - (Vector2)self.position;
    float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + angleOffset;
    return Quaternion.AngleAxis(angle, Vector3.forward);
  }

  public static IEnumerator MoveTo(this Transform transform, Vector2 destination, float speed = 1f)
  {
    //Vector2 start = transform.position;
    while ((destination - (Vector2)transform.position).magnitude > 0.0001f)
    {
      transform.position = Vector2.MoveTowards(transform.position, destination, Time.deltaTime * speed);
      yield return null;
    }
    transform.position = destination;
  }

  public static IEnumerator TurnToLook(this Transform transform, Vector2 destination, float time = 0.25f, float angleOffset = 0)
  {
    Quaternion original = transform.rotation;
    Quaternion target = transform.TopDownLookAt(destination, angleOffset);
    float elapsed = 0f;
    while (elapsed < time)
    {
      elapsed += Time.deltaTime;
      transform.rotation = Quaternion.Slerp(original, target, elapsed / time);
      yield return null;
    }

    transform.rotation = target;
  }
}