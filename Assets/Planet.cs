﻿using System;
using UnityEngine;

public class Planet : MonoBehaviour
{
  public string Name;

  public static Action<Planet> OnPlanetSelected;

  private bool _isSelected;
  public bool IsSelected
  {
    get => _isSelected;
    private set
    {
      _isSelected = value;
      _selectionCursor.SetActive(_isSelected);
    }
  }

  private SpriteRenderer _sr;
  private GameObject _selectionCursor;

  private void Awake()
  {
    _sr = GetComponent<SpriteRenderer>();
    _selectionCursor = transform.Find("Selection").gameObject;
  }

  private void Update()
  {

  }

  public void Select()
  {
    OnPlanetSelected?.Invoke(this);

    IsSelected = true;
  }

  public void Deselect()
  {
    if (!IsSelected)
      return;

    IsSelected = false;
  }

  private void OnMouseEnter()
  {
    _sr.color = new Color(1, 1, 1, 0.5f);
  }

  private void OnMouseDown()
  {
    if (IsSelected)
    {
      Deselect();
      OnPlanetSelected?.Invoke(null);
    }
    else
      Select();
  }

  private void OnMouseExit()
  {
    _sr.color = new Color(1, 1, 1);
  }
}
