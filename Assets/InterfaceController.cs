﻿using TMPro;
using UnityEngine;

public class InterfaceController : MonoBehaviour
{
  public TextMeshProUGUI SelectedPlanetText;

  private void Start()
  {
    PlanetManager.OnPlanetSelectionChanged += HandlePlanetSelected;

    Init();
  }

  private void Init()
  {
    SelectedPlanetText.text = string.Empty;
  }

  private void HandlePlanetSelected(Planet planet)
  {
    SelectedPlanetText.text = planet?.Name ?? string.Empty;
  }
}
