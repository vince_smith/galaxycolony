﻿using System;
using UnityEngine;

public class PlanetManager : MonoBehaviour
{
  public Planet SelectedPlanet { get; private set; }
  public static Action<Planet> OnPlanetSelectionChanged;

  private Planet[] _planets;

  private void Start()
  {
    _planets = FindObjectsOfType<Planet>();

    Planet.OnPlanetSelected += p =>
    {
      foreach (Planet planet in _planets)
      {
        planet.Deselect();
      }

      SelectedPlanet = p;
      OnPlanetSelectionChanged?.Invoke(p);
    };
  }
}
