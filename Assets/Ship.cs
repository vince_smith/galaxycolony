﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(RotateAroundTarget))]
public class Ship : MonoBehaviour
{
  public float Speed;
  public float EnterOrbitRadius;

  private RotateAroundTarget _rotateAroundTarget;

  private void Start()
  {
    _rotateAroundTarget = GetComponent<RotateAroundTarget>();

    _rotateAroundTarget.enabled = false;
  }

  private Transform _target;

  void Update()
  {
    if (Input.GetMouseButtonDown(0))
    {
      ExitOrbit();

      Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

      StopAllCoroutines();
      StartCoroutine(transform.TurnToLook(mousePos, 0.25f, -90f));

      RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
      var target = hit.transform?.GetComponent<Planet>();
      if (target != null)
      {
        _target = target.transform;
      }
      else
      {
        _target = null;
        StartCoroutine(transform.MoveTo(mousePos, Speed));
      }
    }

    if (_target != null)
    {
      if (!EnterOrbit(_target))
      {
        transform.position = Vector2.MoveTowards(transform.position, _target.position, Time.deltaTime * Speed);
      }
    }
  }

  private bool EnterOrbit(Transform target)
  {
    Vector2 difference = target.position - transform.position;
    if (difference.magnitude > EnterOrbitRadius)
      return false;

    Vector2 lookDirection = Vector2.Perpendicular(difference);
    transform.rotation = transform.TopDownLookAt((Vector2)transform.position + lookDirection, 90f);

    float degrees = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
    _rotateAroundTarget.SetTimeFromSignedAngle(degrees);
    _rotateAroundTarget.Target = _target;
    _rotateAroundTarget.enabled = true;
    _target = null;

    return true;
  }

  private void ExitOrbit()
  {
    _rotateAroundTarget.Target = null;
    _rotateAroundTarget.enabled = false;
  }
}
