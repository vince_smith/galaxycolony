﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundPoint : MonoBehaviour
{
  public Vector2 Point;
  public float Speed;

  private void Update()
  {
    transform.RotateAround(Point, Vector3.forward, Time.deltaTime * Speed);
  }
}
